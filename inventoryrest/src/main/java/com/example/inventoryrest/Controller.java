package com.example.inventoryrest;

import com.example.inventoryrest.entity.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequiredArgsConstructor
public class Controller {

    private final KafkaProducer kafkaProducer;

    @GetMapping("/products")
    ResponseEntity<ArrayList<String>> getProducts() {
        ArrayList<String> products = new ArrayList<>();
        products.add("IPhone8");
        products.add("Samsung10");
        products.add("Coffee");
        return ResponseEntity.ok(products);
    }

    @GetMapping("/kafka")
    void getKafka() {
        ArrayList<Product> products = new ArrayList<>();
        products.add(Product.builder().name("Product1").build());
        products.add(Product.builder().name("Product2").build());
        products.add(Product.builder().name("Product3").build());
        products.forEach(kafkaProducer::sendMessage);
    }
}
