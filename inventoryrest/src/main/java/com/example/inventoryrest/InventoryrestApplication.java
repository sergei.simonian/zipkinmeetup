package com.example.inventoryrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventoryrestApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryrestApplication.class, args);
	}

}
