package com.example.store;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;

@FeignClient(name = "inventory", url = "http://localhost:8081")
public interface InventoryFeignClient {

    @GetMapping(value = "/products")
    ArrayList<String> getProducts();

    @GetMapping(value = "/kafka")
    void getKafka();
}
