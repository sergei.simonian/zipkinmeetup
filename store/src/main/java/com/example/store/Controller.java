package com.example.store;

import com.example.inventoryrest.entity.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequiredArgsConstructor
public class Controller {

    private final InventoryFeignClient inventoryFeignClient;

    public static ArrayList<Product> products = new ArrayList<>();

    @GetMapping("/products")
    public ResponseEntity<ArrayList<String>> getProducts() {
        return ResponseEntity.ok(inventoryFeignClient.getProducts());
    }

    @GetMapping("/kafka")
    void requestKafka() {
        inventoryFeignClient.getKafka();
    }

    @GetMapping("/getkafka")
    ResponseEntity<ArrayList<Product>> getKafka() {
        return ResponseEntity.ok(products);
    }
}
