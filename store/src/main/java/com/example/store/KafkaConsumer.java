package com.example.store;

import com.example.inventoryrest.entity.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumer {

    @KafkaListener(topics = "product", groupId = "product")
    public void consume(ConsumerRecord<String, Product> consumerRecord) {
        log.debug("Message received -> {}", consumerRecord.toString());
        Controller.products.add(consumerRecord.value());
    }

//    @KafkaListener(topics = "product")
//    public void listenWithHeaders(
//            @Payload Product product,
//            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
//        Controller.products.add(product);
//    }
}
